# uc-os-ii-code-notes

🐤 Notes of reading μC/OS-II code.

Created by : Mr Dk.

2019 / 11 / 04 @Nanjing, P.R.China

---

I have been curious for a long time about the differences between embedded operating systems and general OS like Linux.

Since I have understood how Linux works on a high level (very very high and abstract 😅), I'm turning to embedded RTOS now.

The book I choose is _嵌入式实时操作系统 μC/OS-II 教程 (第二版)_ by _程文娟、吴永忠、苗刚中_

It will be easier for me to understand a Chinese book.

Move on! 😈

---

## License

Copyright © 2019, Jingtang Zhang. ([MIT License](LICENSE))

---

